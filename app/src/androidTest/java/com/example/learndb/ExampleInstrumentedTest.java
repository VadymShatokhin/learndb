package com.example.learndb;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;

import com.example.learndb.room.RoomsRepository;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleInstrumentedTest {
  private static Repository repository;

  @BeforeClass
  public static void setup() {
    Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
    repository = new ArrayListRepository();
    repository = new OpenHelperRepository(appContext);
    repository = new RoomsRepository(appContext);
    repository = new KotlinArrayRepository();


  }

  @Before
  public void setupBeforeEach() {
    repository.deleteAllUsers();
  }

  @Test
  public void testNonExistentUser() {
    User user = repository.getUserById(234234);
    assertNull(user);

  }

  /**
   * TODO:
   * <p>
   * <p>
   * edit test
   * delete test
   * select all test
   * delete all before each test
   * delete all test
   */
  @Test
  public void testDeleteUser() {
    User user = new User("John", 23);
    repository.saveUser(user);
    // User from db must have same fields as user we created
    assertTrue(repository.deleteUserById(user.getId()));
    User dbUser = repository.getUserById(user.getId());
    assertNull(dbUser);
  }

  @Test
  public void testDeleteNonExistUser() {
    assertFalse(repository.deleteUserById(9999999));
  }

  @Test
  public void testCreateAndReadUser() {
    User user = new User("John", 23);

    // User should not have any id
    assertEquals(user.getId(), User.NO_ID);

    repository.saveUser(user);
    // User should have valid id
    assertNotEquals(user.getId(), User.NO_ID);

    // User from db must have same fields as user we created
    User dbUser = repository.getUserById(user.getId());
    assertEquals(user.getName(), dbUser.getName());
    assertEquals(user.getAge(), dbUser.getAge());
  }

  @Test
  public void testEditUser() {
    User user = new User("John", 23);

    // User should not have any id
    assertEquals(user.getId(), User.NO_ID);

    repository.saveUser(user);
    // User should have valid id
    assertNotEquals(user.getId(), User.NO_ID);

    user.setName("Ksu");
    repository.saveUser(user);

    // User from db must have same fields as user we created
    User dbUser = repository.getUserById(user.getId());
    assertEquals(user.getName(), dbUser.getName());
    assertEquals("Ksu", dbUser.getName());
    assertEquals(user.getAge(), dbUser.getAge());
  }

  @Test
  public void testSelectNoUsers() {
    List<User> allUsers = repository.getAllUsers();
    assertTrue(allUsers.isEmpty());
  }

  @Test
  public void testSelectOneUser() {
    User alex = new User("Alex", 11);
    repository.saveUser(alex);
    List<User> allUsers = repository.getAllUsers();
    assertEquals(1, allUsers.size());
    assertEquals(alex, allUsers.get(0));
  }

  @Test
  public void testAdults() {
    repository.saveUser(new User("Alex", 17));
    repository.saveUser(new User("Diana", 33));
    repository.saveUser(new User("BoJack", 42));
    repository.saveUser(new User("tony", 11));
    List<User> userList = repository.getAdultUsers();
    assertEquals(2, userList.size());
    assertTrue(isAdultUser(userList.get(0)));
    assertTrue(isAdultUser(userList.get(1)));

  }
  private boolean isAdultUser(User user){
    return user.getName().equals("Diana") || user.getName().equals("BoJack");
  }


}