package com.example.learndb;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

class ArrayListRepository implements Repository {
    private ArrayList<User> db = new ArrayList<>();
    private long nextUserId = 0L;

    @Nullable
    @Override
    public User getUserById(long id) {
        for (User user : db) {
            if (user.getId() == id) return user;
        }

        return null;
    }

    @Override
    public void saveUser(@NonNull User user) {
        for (User dbUser : db) {
            if (user.getId() == dbUser.getId()) {
                db.remove(dbUser);
                db.add(user);
                return;
            }
        }
        user.setId(nextUserId++);
        db.add(user);
    }

    @Override
    public boolean deleteUserById(long id) {
        int deleteIndex = -1;
        for (User dbUser : db) {
            if (id == dbUser.getId()) {
                deleteIndex = db.indexOf(dbUser);
                break;
            }
        }
        if (deleteIndex != -1) {
            db.remove(deleteIndex);
            return true;
        }

        return false;
    }

    @NonNull
    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(db);
    }

    @Override
    public boolean deleteAllUsers() {
        if (!db.isEmpty()) {
            db.clear();
            return true;
        }
        return false;
    }

    @Override
    public List<User> getAdultUsers() {
        List<User> adultList = new ArrayList<>();
        for (User user : db) {
            if (user.getAge() > 18) {
                adultList.add(user);
            }
        }
        return adultList;
    }
}
