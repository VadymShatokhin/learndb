package com.example.learndb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class OpenHelperRepository implements Repository {
  private final SQLiteDatabase db;

  public OpenHelperRepository(Context context) {
    db = new DbOpenHelper(context).getWritableDatabase();
  }


  @Override
  @Nullable
  public User getUserById(long id) {
    Cursor cursor = db.query("users", null, "id = " + id, null, null, null, null);
    List<User> users = toUsers(cursor);
    if (!users.isEmpty()) return users.get(0);
    return null;
  }

  @Override
  public void saveUser(@NonNull User user) {
    ContentValues cv = new ContentValues();
    cv.put("name", user.getName());
    cv.put("age", user.getAge());

    if (user.getId() == User.NO_ID) {
      long id = db.insert("users", null, cv);
      user.setId(id);
    } else {
      db.update("users", cv, "id = " + user.getId(), null);
    }
  }

  @Override
  public boolean deleteUserById(long id) {
    return db.delete("users", "id= ?", new String[]{id + ""}) > 0;
  }

  @Override
  @NonNull
  public List<User> getAllUsers() {
    Cursor cursor = db.query("users", null, null, null, null, null, null);
    return toUsers(cursor);
  }

  private User toUser(Cursor cursor) {
    String name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
    int age = cursor.getInt(cursor.getColumnIndexOrThrow("age"));
    int id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
    return new User(id, name, age);
  }

  /**
   * @return true if any elements where deleted
   */
  @Override
  public boolean deleteAllUsers() {
    return db.delete("users", null, null) > 0;
  }

  @Override
  public List<User> getAdultUsers() {
    Cursor cursor = db.query("users", null, "age > 18", null, null, null, null);
    return toUsers(cursor);
  }

  @NotNull
  private List<User> toUsers(Cursor cursor) {
    List<User> userList = new ArrayList<>();
    while (cursor.moveToNext()) {
      userList.add(toUser(cursor));
    }
    cursor.close();
    return userList;
  }


}
