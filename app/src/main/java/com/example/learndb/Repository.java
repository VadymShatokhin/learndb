package com.example.learndb;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public interface Repository {
    @Nullable
    User getUserById(long id);

    /**
     * Create new user with unic ID or updates exiting user
     * don't forget to set if to original user
     * @param user
     */
    void saveUser(@NonNull User user);

    boolean deleteUserById(long id);

    @NonNull
    List<User> getAllUsers();

    boolean deleteAllUsers();

    List<User> getAdultUsers();
}
