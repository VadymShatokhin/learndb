package com.example.learndb.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


@Dao
 public interface RoomDao {


    @Insert
     long insertUser (UserEntity entity);

    @Delete
    void deleteUser (UserEntity entity);

    @Update
    void updateUser (UserEntity entity);

    @Query("SELECT * FROM UserEntity WHERE id = :id")
    UserEntity getUserById(long id);

    @Query("SELECT * FROM UserEntity")
    List<UserEntity> getAllUsers();

   @Query("DELETE FROM UserEntity")
    int deleteAllUsers();

    @Query("DELETE FROM UserEntity WHERE id = :id")
    int deleteUserById(long id);

    @Query("SELECT * FROM UserEntity WHERE age > 18")
    List<UserEntity> getAdultUsers();
}
