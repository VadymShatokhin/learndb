package com.example.learndb.room;

import androidx.room.Database;

@Database(entities ={UserEntity.class}, version = 1)
abstract class RoomDatabase extends androidx.room.RoomDatabase {
    public abstract RoomDao roomDao();

}
