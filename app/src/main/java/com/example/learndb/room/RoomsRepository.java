package com.example.learndb.room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import com.example.learndb.Repository;
import com.example.learndb.User;

import java.util.ArrayList;
import java.util.List;

public class RoomsRepository implements Repository {
    RoomDatabase db;

    public RoomsRepository(Context context) {
        db = Room.databaseBuilder(context, RoomDatabase.class, "database").build();

    }

    @Nullable
    @Override
    public User getUserById(long id) {
        UserEntity userEntity = db.roomDao().getUserById(id);
            return toUser(userEntity);
    }

    @Override
    public void saveUser(@NonNull User user) {
        long id = db.roomDao().insertUser(
                new UserEntity(user.getName(), user.getAge()));
        user.setId(id);
    }

    @Override
    public boolean deleteUserById(long id) {
        return db.roomDao().deleteUserById(id) > 0;
    }

    @NonNull
    @Override
    public List<User> getAllUsers() {
        return formUsersEntityToUsers(db.roomDao().getAllUsers());
    }

    @Override
    public boolean deleteAllUsers() {
        int numOdDeleted = db.roomDao().deleteAllUsers();
        return numOdDeleted > 0;
    }

    @Override
    public List<User> getAdultUsers() {
        return formUsersEntityToUsers(db.roomDao().getAdultUsers());
    }

    public List<User> formUsersEntityToUsers(List<UserEntity> userEntityList) {
        List<User> userList = new ArrayList<>();
        for (UserEntity userEntity : userEntityList) {
            userList.add(toUser(userEntity));
        }
        return userList;
    }

    @Nullable
    private User toUser(@Nullable UserEntity userEntity) {
        if (userEntity != null) {
            return new User(userEntity.id, userEntity.name, userEntity.age);
        } else return null;
    }
}
