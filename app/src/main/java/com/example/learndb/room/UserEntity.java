package com.example.learndb.room;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
class UserEntity {
    @PrimaryKey (autoGenerate = true)
    public long id;
    public String name;
    public int age;

    public UserEntity(String name, int age) {
        this.name = name;
        this.age = age;
    }
    @Ignore
    public UserEntity(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}
