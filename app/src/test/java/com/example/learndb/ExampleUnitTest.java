package com.example.learndb;

import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
  @Test
  public void addition_isCorrect() {
    List<Integer> integers = Arrays.asList(5, 4, 3);
    for(Integer integer : integers) {
      System.out.println("Int: " + integer);
    }

    printIterable(integers);
  }

  private void printIterable(Iterable iterable) {
    Iterator iterator = iterable.iterator();
    try {
      while(true) {
        System.out.println(iterator.next());
      }
    } catch (NoSuchElementException e) {
      // pass
    }
  }
}